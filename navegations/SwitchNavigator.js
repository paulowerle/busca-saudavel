import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import Login from '../pages/Login';
import TabNavegator from './TabNavegator';

const SwitchNavigator = createSwitchNavigator({
    Login,
    TabNavegator,
});

export default createAppContainer(SwitchNavigator);