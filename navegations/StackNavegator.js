import React from "react";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MatchesScreen from "../pages/Matches";
import MatcheProfile from "../pages/MatcheProfile";

const StackNavegation = createStackNavigator({
    MatchesScreen: {
        screen: MatchesScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    MatcheProfile: {
        screen: MatcheProfile,
        navigationOptions: {
            headerShown: false
        }
    }
});

export default createAppContainer(StackNavegation);
