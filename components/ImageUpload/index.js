import React, { useState } from 'react';
import * as ImagePicker from 'expo-image-picker';

import {
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import globalStyles from '../../assets/styles';
import styles from './style';

const ImageUpload = ({image}) => {

    const [image_update, setImageUpdate] = useState(image);
  
    const pickImage = async () => {
        
        // Permission
        async function handlePermission() {
            if (Platform.OS !== 'web') {
                const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
                if (status !== 'granted') {
                    alert('Desculpe, mas precisamos da permissão para acessar sua camera');
                }
            }
        };
        handlePermission();

        // Image
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
  
        // if (!result.cancelled) {
        //     async function updateImage() {
        //         const response = await api.post( usersRoute, {
        //             headers: { user_id: user },
        //             data: { image_uri: result.uri }
        //         })

        //         if (!!response.data){
        //             setImageUpdate(result.uri);
        //         }

        //     };
        //     updateImage();
        // }
    };
  
    return(
        // <ImageBackground source={ {uri: image_update} } style={styles.photo}>
        <ImageBackground source={ image_update } style={globalStyles.photo}>
            <View style={styles.containerImageUpdate}>
                <TouchableOpacity
                    style={styles.btnImageUpdate}
                    onPress={pickImage}
                >
                    <Text style={styles.textBtnImageUpdate}>
                        Alterar imagem
                    </Text>
                    <Icon
                        style={styles.iconBtnImageUpdate}
                        name="cloud-upload"
                    />
                </TouchableOpacity>
            </View>
        </ImageBackground>
    );
};

export default ImageUpload;