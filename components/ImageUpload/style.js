import { StyleSheet } from "react-native";
import {
	SECONDARY_COLOR,
	WHITE,
} from "../../assets/styles/variables";

export default StyleSheet.create({
    containerImageUpdate: {
        position: "absolute",
        bottom: 75,
        alignItems: "center",
        left: 0,
        right: 0,
        margin: "auto"
    },
    btnImageUpdate: {
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 25,
        backgroundColor: SECONDARY_COLOR,
    },
    textBtnImageUpdate: {
        color: WHITE,
        fontSize: 20
    },
    iconBtnImageUpdate: {
        color: WHITE,
        fontSize: 25,
    },
})