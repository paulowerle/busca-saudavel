import { StyleSheet } from "react-native";
import { 
	WHITE,
	BLACK,
	GREEN,
} from "../../assets/styles/variables";

export default StyleSheet.create({ 
    contentInputProfile: {
        marginVertical: 10
	},
    labelProfile:{
        fontWeight: "400",
	},
	inputTextProfile: {
        height: 40,
        width: 'auto',
        borderBottomColor: BLACK,
        borderBottomWidth: 1
    },
    inputTextAreaProfile: {
        height: 80,
        width: 'auto',
        borderBottomColor: BLACK,
        borderBottomWidth: 1
    },
    btnSaveProfile: {
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: GREEN,
        borderRadius: 25,
        color: WHITE
    },
    textSaveProfile: {
        color: WHITE,
        fontWeight: "bold"
	},
})