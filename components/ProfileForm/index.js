import React, { useState } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import api from '../../services/api';
import { updateProfile } from '../../routes'
import { Storage, StorageKeys } from '../../services/storage';

import styles from './style';

const ProfileForm = ({ name, age, facebook, instagram, phone, modalidade, bio }) => {

    const [input_name, setInputName] = useState(name);
    const [input_age, setInputAge] = useState(age);
    const [input_phone, setInputPhone] = useState(phone);
    const [input_facebook, setInputFacebook] = useState(facebook);
    const [input_instagram, setInputInstagram] = useState(instagram);
    const [input_modalidade, setInputModalidade] = useState(modalidade);
    const [input_bio, setInputBio] = useState(bio);
    
    const handleSave = async () => {
        const storedUser = await Storage.getData(StorageKeys.User.Info);
        const data = {
            nome: input_name,
            idade: input_age,
            telefone: input_phone,
            facebook: input_facebook,
            instagram: input_instagram,
            modalidade: input_modalidade,
            bio: input_bio,
        };

        const response = await api.put(updateProfile, data, {
          headers: {
            id: storedUser._id
          }
        });

        if(!response.data.status){
            window.alert('Falha: ' + (response.data.message || 'Genérico!'));
            return;
        }

        window.alert('Sucesso');
    }

    return (
        <View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Nome</Text>
                <TextInput
                    style={styles.inputTextProfile}
                    placeholder="Digite aqui seu nome"
                    onChangeText={text => setInputName(text)}
                    defaultValue={input_name}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Idade</Text>
                <TextInput
                    style={styles.inputTextProfile}
                    placeholder="Digite aqui sua idade"
                    onChangeText={text => setInputAge(text)}
                    defaultValue={input_age}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Telefone</Text>
                <TextInput
                    style={styles.inputTextProfile}
                    placeholder="Digite aqui seu telefone"
                    onChangeText={text => setInputPhone(text)}
                    defaultValue={input_phone}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Link do Facebook</Text>
                <TextInput
                    style={styles.inputTextProfile}
                    placeholder="Cole aqui seu link do facebook"
                    onChangeText={text => setInputFacebook(text)}
                    defaultValue={input_facebook}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Link do Instagram</Text>
                <TextInput
                    style={styles.inputTextProfile}
                    placeholder="Cole aqui seu link do Instagram"
                    onChangeText={text => setInputInstagram(text)}
                    defaultValue={input_instagram}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Modalidade</Text>
                <TextInput
                    style={styles.inputTextProfile}
                    placeholder="Digite aqui sua modalidade"
                    onChangeText={text => setInputModalidade(text)}
                    defaultValue={input_modalidade}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <Text style={styles.labelProfile} >Bio</Text>
                <TextInput
                    multiline
                    style={styles.inputTextAreaProfile}
                    placeholder="Digite aqui sua Bio"
                    onChangeText={text => setInputBio(text)}
                    defaultValue={input_bio}
                />
            </View>
            <View style={styles.contentInputProfile} >
                <TouchableOpacity style={styles.btnSaveProfile} onPress={() => handleSave()} >
                    <Text style={styles.textSaveProfile} > Salvar </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default ProfileForm;