import React from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './style';

const ProfileItem = ({
  age,
  bio,
  modalidade,
  name
}) => {
  return (
    <View style={styles.containerProfileItemMatches}>
      <Text style={styles.name}>{name}</Text>

      <Text style={styles.descriptionProfileItem}>
        {age}
      </Text>

      <View style={styles.info}>
        <Text style={styles.iconProfile}>
          <Icon name="hashtag" />
        </Text>
        <Text style={styles.infoContent}>{modalidade}</Text>
      </View>

      <View style={styles.info}>
        <Text style={styles.iconProfile}>
          <Icon name="circle" />
        </Text>
        <Text style={styles.infoContent}>{bio}</Text>
      </View>
    </View>
  );
};

export default ProfileItem;
