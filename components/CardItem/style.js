import { StyleSheet } from "react-native";
import { 
	PRIMARY_COLOR,
	WHITE,
	GRAY,
	DARK_GRAY,
	BLACK,
	GREEN,
	RED,
	STAR_ACTIONS,
	LIKE_ACTIONS,
	DISLIKE_ACTIONS,
	FLASH_ACTIONS,
} from "../../assets/styles/variables";


export default StyleSheet.create({
    containerCardItem: {
        backgroundColor: WHITE,
        borderRadius: 8,
        alignItems: "center",
        margin: 10,
        shadowOpacity: 0.05,
        shadowRadius: 10,
        shadowColor: BLACK,
        shadowOffset: { height: 0, width: 0 }
    },
    descriptionCardItem: {
        color: GRAY,
        textAlign: "center"
    },
    status: {
        paddingBottom: 10,
        flexDirection: "row",
        alignItems: "center"
    },
    statusText: {
        color: GRAY,
        fontSize: 12
    },
    online: {
        width: 6,
        height: 6,
        backgroundColor: GREEN,
        borderRadius: 3,
        marginRight: 4
    },
    offline: {
        width: 6,
        height: 6,
        backgroundColor: RED,
        borderRadius: 3,
        marginRight: 4
    },
    actionsCardItem: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 30
    },
    button: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: WHITE,
        marginHorizontal: 7,
        alignItems: "center",
        justifyContent: "center",
        shadowOpacity: 0.15,
        shadowRadius: 20,
        shadowColor: DARK_GRAY,
        shadowOffset: { height: 10, width: 0 }
    },
    miniButton: {
        width: 40,
        height: 40,
        borderRadius: 30,
        backgroundColor: WHITE,
        marginHorizontal: 7,
        alignItems: "center",
        justifyContent: "center",
        shadowOpacity: 0.15,
        shadowRadius: 20,
        shadowColor: DARK_GRAY,
        shadowOffset: { height: 10, width: 0 }
    },
    star: {
        color: STAR_ACTIONS
    },
    like: {
        fontSize: 25,
        color: LIKE_ACTIONS
    },
    dislike: {
        fontSize: 25,
        color: DISLIKE_ACTIONS
    },
    flash: {
        color: FLASH_ACTIONS
    },
})