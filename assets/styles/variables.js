import { Dimensions } from "react-native";

export const PRIMARY_COLOR =    "#7444C0";
export const SECONDARY_COLOR =  "#5636B8";
export const WHITE =            "#FFFFFF";
export const BLACK =            "#000000";
export const GRAY =             "#757E90";
export const GREEN =            "#228b22";
export const RED =              "#cc0000";

export const DARK_GRAY =        "#363636";

export const STAR_ACTIONS =     "#FFA200";
export const LIKE_ACTIONS =     "#B644B2";
export const DISLIKE_ACTIONS =  "#363636";
export const FLASH_ACTIONS =    "#5028D7";

export const DIMENSION_WIDTH = Dimensions.get("window").width;
export const DIMENSION_HEIGHT = Dimensions.get("window").height;