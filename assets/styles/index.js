import { StyleSheet } from "react-native";
import { 
	DARK_GRAY,
	DIMENSION_WIDTH,
	DIMENSION_HEIGHT,
} from "./variables";

export default StyleSheet.create({
	// GERAL
	bg: {
		flex: 1,
		resizeMode: "cover",
		width: DIMENSION_WIDTH,
		height: DIMENSION_HEIGHT
	},
	top: {
		paddingTop: 50,
		marginHorizontal: 10,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	title: { 
		paddingBottom: 10, 
		fontSize: 22, 
		color: DARK_GRAY 
	},
	icon: {
		fontSize: 20,
		color: DARK_GRAY,
		paddingRight: 10
	},
	photo: {
        width: DIMENSION_WIDTH,
        height: 450
	},
	
	// MENU
	tabButton: {
		paddingTop: 20,
		paddingBottom: 30,
		alignItems: "center",
		justifyContent: "center",
		flex: 1
	},
	tabButtonText: {
		textTransform: "uppercase"
	},
	iconMenu: {
		height: 20,
		paddingBottom: 7
	},
});
