
export const StorageKeys = Object.freeze({
    User: {
        Info: 'user.indo'
    },
});


export const Storage = {
    async getData(key) {
        let data = {};

        try {
            data = JSON.parse(localStorage.getItem(key));
        } catch (error) {
            console.error(error);
        }

        return data;
    },
    async setData(key, value) {
        try {
            localStorage.setItem(key, JSON.stringify(value));
        } catch (error) {
            console.error(error);
        }
    },
    async removeData(key){
        try {
            localStorage.removeItem(key);
        } catch (error) {
            console.error(error);
        }
    }
};