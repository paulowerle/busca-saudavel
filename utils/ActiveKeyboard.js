import React from 'react';
import {
    KeyboardAvoidingView,
    Platform
} from 'react-native';

const ActiveKeyboard = ({children}) => {
    return(
        <KeyboardAvoidingView
            enabled 
            behavior={ Platform.OS === 'ios'? 'padding': null}
        >
            {children}
        </KeyboardAvoidingView>
    )
}

export default ActiveKeyboard;