import React from 'react';
import SwitchNavigator from "./navegations/SwitchNavigator"
import { RecoilRoot } from "recoil";
const App = () => {
    return(
		<RecoilRoot>
			<SwitchNavigator />
		</RecoilRoot>
	)
}

export default App;