import { StyleSheet } from "react-native";
import { WHITE, DIMENSION_WIDTH, BLACK } from "../../assets/styles/variables";

export default StyleSheet.create({
    contentLogin: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: DIMENSION_WIDTH / 1.2,
        height: 250,
        marginBottom: 15,
    },
    btnFacebook: {
        marginBottom: 15,
        padding: 15,
        borderRadius: 30,
        backgroundColor: '#3b5998',
        flexDirection: 'row',
        justifyContent: "center",
        shadowColor: BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    textFacebook: {
        color: WHITE,
        fontSize: 15,
    },
    iconFacebook: {
        color: WHITE,
        fontSize: 20,
        marginEnd: 15
    },
    btnGoogle: {
        padding: 15,
        borderRadius: 30,
        backgroundColor: WHITE,
        flexDirection: 'row',
        justifyContent: "center",
        shadowColor: BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    textGoogle: {
        color: BLACK,
        fontSize: 15,
    },
    iconGoogle: {
        color: '#dd4e37',
        fontSize: 20,
        marginEnd: 15
    }
})