import React, { useState, useRef, useEffect } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    View,
    Text,
    Image,
    Alert,
    Platform,
} from 'react-native';
import Dialog from "react-native-dialog";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useRecoilState } from "recoil";
import { userState } from "../../utils/atoms";

import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import api from '../../services/api';
import { Storage, StorageKeys } from '../../services/storage';
import { usersRoute, authByEmail } from '../../routes'

import styles from './style'
import { Demo } from '../../assets/data/demo.js';


const Login = (props) => {
    const [user, setUser] = useRecoilState(userState);
    const [showFakeLogin, setShowFakeLogin] = useState(false);
    const fakeLoginInput = useRef(null);


    useEffect(() => {
        const runner = async () => {
            const storedUser = await Storage.getData(StorageKeys.User.Info);

            if (storedUser && storedUser._id) {
                navigateToHome(storedUser);
            }
        };
        runner();
    }, []);

    const successLogin = async (userData) => {
        await Storage.setData(StorageKeys.User.Info, userData);
        setUser(userData);
        navigateToHome(userData);
    }

    const navigateToHome = user => {
        props.navigation.navigate("Explore", user);
    }

    const error = (msg) => {
        Alert.alert(
            "Erro", (msg || "Não foi possivel fazer login, tente novamente mais tarde"),
            [{ text: "OK" }],
            { cancelable: false }
        );
    }

    // const login = (token) => {
    //     const response = await api.post(usersRoute, {
    //         headers: { token: token }
    //     })
    //     if (!!response.data) {
    //         setUser(response.data)
    //         props.navigation.navigate("Explore", user)
    //     }
    // }

    const FakeLoginDialog = () => {
        return (
            <Dialog.Container
                visible={showFakeLogin}
                style={{
                    flex: 1,
                    width: "100%",
                    borderColor: "transparent"
                }}
            >
                <Dialog.Title>Login</Dialog.Title>
                <Dialog.Description>
                    Informe o e-mail para logar
                    </Dialog.Description>
                <Dialog.Input
                    label="E-mail"
                    textInputRef={(input) => fakeLoginInput.current = input}
                />
                <Dialog.Button label="Confirmar" onPress={handleFakeLogin} />
            </Dialog.Container>
        );
    }

    const handleFakeLogin = async () => {
        setShowFakeLogin(false);
        const email = fakeLoginInput.current.value;
        const response = await api.post(authByEmail, {
            email
        });

        if (!response.data.status) {
            error(response.data.message);
            return;
        }

        successLogin(response.data.user);
    }

    const handleGoogle = async () => {
        // if (Platform.OS == "web") {
        return setShowFakeLogin(true);
        // }

        // const { type, accessToken, user } = await Google.logInAsync({
        //     iosClientId: '88966188754-amsubtk9kkilappaar0b7tbm80qeae0u.apps.googleusercontent.com',
        //     androidClientId: '88966188754-6qq3ncnosalnankkeom024ebecltbab6.apps.googleusercontent.com',
        //     scopes: ["profile", "email"]
        // });

        // if (type === 'success') {
        //     login(accessToken)

        //     // Acredito que esse cara vai na api
        //     // let userInfoResponse = await fetch('https://www.googleapis.com/userinfo/v2/me', {
        //     //     headers: { Authorization: `Bearer ${accessToken}` },
        //     // });
        // } else {
        //     error();
        // }
    }

    const handleFacebook = async () => {
        // if (Platform.OS == "web") {
        return setShowFakeLogin(true);
        // }

        // await Facebook.initializeAsync({ appId: '213557760366378' });
        // const {
        //     type,
        //     token,
        //     expirationDate,
        //     permissions,
        //     declinedPermissions,
        // } = await Facebook.logInWithReadPermissionsAsync({
        //     permissions: ['public_profile'],
        // });

        // if (type === 'success') {
        //     login(token);

        //     // Acredito que esse cara vai na api
        //     // const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        // } else {
        //     error();
        // }
    }

    return (
        <>
            {showFakeLogin && <FakeLoginDialog />}
            {!showFakeLogin && (
                <View style={styles.contentLogin} >
                    <Image
                        source={require('../../assets/images/login.png')}
                        style={styles.image}
                    />
                    <TouchableOpacity
                        style={styles.btnFacebook}
                        onPress={handleFacebook}
                    >
                        <Icon
                            style={styles.iconFacebook}
                            name="facebook"
                        />
                        <Text style={styles.textFacebook}> Fazer login com Facebook</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.btnGoogle}
                        onPress={handleGoogle}
                    >
                        <Icon
                            style={styles.iconGoogle}
                            name="google"
                        />
                        <Text style={styles.textGoogle}> Fazer login com Google</Text>
                    </TouchableOpacity>
                </View>
            )}
        </>
    )
}

export default Login;