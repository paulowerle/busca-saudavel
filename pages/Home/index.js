import React, { useState, useEffect } from 'react';
import { View, ImageBackground, Image, Text } from 'react-native';
import CardStack, { Card } from 'react-native-card-stack-swiper';

import api from '../../services/api';
import { getNextPerson, likePerson } from '../../routes'

import globalStyles from '../../assets/styles';
import styles from './style';
import CardItem from '../../components/CardItem';
import { Storage, StorageKeys } from '../../services/storage';
import { useCallback } from 'react';

const Home = () => {
  const [users, setUsers] = useState([]);
  const [swiper, setSwiper] = useState(null);

  useEffect(() => {
    const runner = async () => {
      const storedUser = await Storage.getData(StorageKeys.User.Info);

      if (!storedUser)
        navigateToLogin(storedUser);

      getNetxPerson();
    }

    runner();
  }, []);

  const getNetxPerson = async () => {
    const storedUser = await Storage.getData(StorageKeys.User.Info);
    const nextMatch = await api.get(getNextPerson, {
      headers: {
        id: storedUser._id
      }
    });

    if (nextMatch.data.data == null)
      return;
      
    setTimeout(() => {
      setUsers([nextMatch.data.data]);
    }, 500);
  };

  const navigateToLogin = () => {
    props.navigation.navigate("Login");
  };

  const handleLike = async () => {
    const storedUser = await Storage.getData(StorageKeys.User.Info);
    await api.post(likePerson + users[0]._id, { like: true }, {
      headers: {
        id: storedUser._id
      }
    });
    setUsers([]);
    getNetxPerson();
  };

  const handleDislike = async () => {
    const storedUser = await Storage.getData(StorageKeys.User.Info);
    await api.post(likePerson + users[0]._id, { like: false }, {
      headers: {
        id: storedUser._id
      }
    });
    setUsers([]);
    getNetxPerson();
  };

  const handleLikeButton = () => {
    swiper.swipeLeft();
    setTimeout(() => {
      handleLike();
    }, 300);
  };

  const handleDislikeButton = () => {
    swiper.swipeRight();
    setTimeout(() => {
      handleDislike();
    }, 300);
  };

  const NoMoreCards = () => {
    return (
      <View style={{
        alignItems: 'center',
        marginTop: '50px',
        fontSize: 18,
      }}>
        <Text>Nenhuma pessoa encontrada</Text>
        <Text>Não encontramos mais nenhuma pessoa compatível</Text>
      </View>
    )
  };


  const CardStackRef = useCallback(() => {
    return (
      <CardStack
        loop={false}
        onSwipedLeft={() => handleLike()}
        onSwipedRight={() => handleDislike()}
        verticalSwipe={false}
        renderNoMoreCards={() => <NoMoreCards />}
        ref={_swiper => (setSwiper(_swiper))}
      >
        {users.map((item, index) => (
          <Card key={index}>
            <CardItem
              image={item.image}
              name={item.nome}
              description={item.bio}
              actions
              onPressLeft={() => handleLikeButton()}
              onPressRight={() => handleDislikeButton()}
            />
          </Card>
        ))}
      </CardStack>
    );
  }, [users])

  return (
    <ImageBackground
      source={require('../../assets/images/bg.png')}
      style={globalStyles.bg}
    >
      <View>
        <Image
          source={require('../../assets/images/icon.png')}
          style={{
            flex: 1,
            height: 40,
            width: 40,
            alignSelf: "center",
            marginTop: 20,
            marginBottom: 10,
          }}
        />
      </View>
      <View style={styles.containerHome}>
        <CardStackRef />
      </View>
    </ImageBackground>
  );
};

export default Home;
