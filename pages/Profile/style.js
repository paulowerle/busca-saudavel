import { StyleSheet } from "react-native";
import {
    WHITE,
    BLACK,
    RED,
} from "../../assets/styles/variables";

export default StyleSheet.create({
    containerProfile: {
        flexDirection: 'column'
    },
    containerProfileItem: {
        backgroundColor: WHITE,
        padding: 25,
        margin: 20,
        borderRadius: 8,
        marginTop: -65,
        shadowOpacity: 0.05,
        shadowRadius: 10,
        shadowColor: BLACK,
        shadowOffset: { height: 0, width: 0 }
    },
    containerProfileAvaliation: {
        margin: 10
    },
    containerProfileDate: {
        marginLeft: 10,
        marginRight: 10
    },
    containerProfileDateText: {
        fontSize: 12
    },
    containerProfileRating: {
        alignItems: 'flex-start',
        marginTop: 15,
        marginBottom: 5,
    },
    btnProfileDelete: {
        backgroundColor: RED,
        paddingVertical: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        marginHorizontal: 20,
        marginBottom: 20
    },
    textProfileDelete: {
        color: WHITE,
        fontWeight: "bold"
    }
})