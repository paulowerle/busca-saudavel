import React, { useState, useEffect, useCallback } from 'react';
import { Rating } from 'react-native-ratings';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  ScrollView,
  ImageBackground,
  Linking,
  Text,
  View
} from 'react-native';

import globalStyles from '../../assets/styles';
import styles from './style';
import ProfileForm from '../../components/ProfileForm';
import ImageUpload from '../../components/ImageUpload';
import ActiveKeyboard from '../../utils/ActiveKeyboard';
import api from '../../services/api';
import { userProfile } from '../../routes'
import { Storage, StorageKeys } from '../../services/storage';

const Profile = () => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const runner = async () => {
      const storedUser = await Storage.getData(StorageKeys.User.Info);
      const userData = await api.get(userProfile, {
        headers: {
          id: storedUser._id
        }
      });
      setUser(userData.data.data);
    }

    runner();
  }, []);

  const formatedDate = useCallback(() => {
    if (!user) return '';

    const date = new Date(user.createdAt);

    var mm = date.getMonth() + 1;
    var dd = date.getDate();
    return [(dd > 9 ? '' : '0') + dd, '/', (mm > 9 ? '' : '0') + mm, '/', date.getFullYear()].join('');
  }, [user]);

  function handleComment() {
    Linking.openURL(`https://api.whatsapp.com/send?phone=${number}&text=Olá`)
  }

  return (
    <ImageBackground
      source={require('../../assets/images/bg.png')}
      style={globalStyles.bg}
    >
      {user && (
        <ScrollView style={styles.containerProfile}>
          <ImageUpload
            image={require(`../../assets/images/${user.image}`)}
          />
          <View style={styles.containerProfileItem}>
            <ProfileForm
              name={user.nome}
              age={user.idade}
              phone={user.telefone}
              facebook={user.facebook_url}
              instagram={user.instagram_url}
              modalidade={user.modalidade}
              bio={user.bio}
            />

            <View style={styles.containerProfileAvaliation} >
              <Text> Avaliação do meu perfil:</Text>
              <View style={styles.containerProfileRating}>
                <Rating
                  startingValue={0}
                  readonly={true}
                  imageSize={20}
                />
              </View>
            </View>

            <View style={styles.containerProfileDate} >
              <Text style={styles.containerProfileDateText}> Ativo desde: {formatedDate()}</Text>
            </View>

          </View>

          <TouchableOpacity style={styles.btnProfileDelete}>
            <Text style={styles.textProfileDelete} > Deletar Conta </Text>
          </TouchableOpacity>

        </ScrollView>
      )}
    </ImageBackground>
  );
};

export default Profile;
