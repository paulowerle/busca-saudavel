import React, { useEffect } from 'react';
import { Storage, StorageKeys } from '../../services/storage';


const Login = (props) => {
    useEffect(() => {
        const runner = async () => {
            await Storage.removeData(StorageKeys.User.Info);
            navigateToHome();
        };
        runner();
    }, []);

    const navigateToHome = () => {
        props.navigation.navigate("Login");
    }

    return (
        <>
        </>
    )
}

export default Login;