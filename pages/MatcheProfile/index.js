import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  ScrollView,
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  Linking
} from 'react-native';
import { useState } from 'react';

import globalStyles from '../../assets/styles';
import styles from './style';
import ProfileItem from '../../components/ProfileItem';

const MatcheProfile = ( { navigation } ) => {
  const [user, setUser ] = useState( navigation.getParam('item', {}) );

  console.log(user);
  
  const {
    idade,
    image,
    nome,
    telefone,
    modalidade,
    bio,
  } = user;

  function handleComment(){
    Linking.openURL(`https://api.whatsapp.com/send?phone=${telefone}&text=Olá`)
  }

  return (
    <ImageBackground
      source={require('../../assets/images/bg.png')}
      style={globalStyles.bg}
    >
      <ScrollView style={styles.containerProfileMatches}>
        <ImageBackground source={require(`../../assets/images/${image}`)} style={globalStyles.photo}>
          <TouchableOpacity 
          onPress={() => navigation.goBack()}
            style={styles.stackNavegationContent}
          >
            <Icon 
              style={styles.stackNavegationIcon} 
              name="chevron-left" 
            />
            <Text 
            style={styles.stackNavegationText}
            > 
              AMIGOS
            </Text>
          </TouchableOpacity>
        </ImageBackground>

        <ProfileItem
          name={nome}
          age={idade}
          modalidade={modalidade}
          bio={bio}
        />

        <View style={styles.actionsProfile}>
          <TouchableOpacity 
            style={styles.roundedButton}
            onPress={handleComment}
            >
            <Text style={styles.iconButton}>
              <Icon name="whatsapp" style={styles.iconWhatsapp}/>
            </Text>
            <Text style={styles.textButton}>Chamar no whatsapp</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

export default MatcheProfile;
