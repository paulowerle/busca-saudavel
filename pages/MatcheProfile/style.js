import { StyleSheet } from "react-native";
import { 
	PRIMARY_COLOR,
	SECONDARY_COLOR,
	WHITE
} from "../../assets/styles/variables";

export default StyleSheet.create({
    containerProfileMatches: { 
        marginHorizontal: 0 
    },
    actionsProfile: {
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center"
    },
    iconButton: {  
        fontSize: 25, 
        color: WHITE 
    },
    iconWhatsapp: {
        fontSize: 25
    },	
    textButton: {
        fontSize: 15,
        color: WHITE,
        paddingLeft: 5
    },
    circledButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: PRIMARY_COLOR,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10
    },
    roundedButton: {
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 10,
        height: 50,
        borderRadius: 25,
        backgroundColor: SECONDARY_COLOR,
        paddingHorizontal: 20
    },
    stackNavegationContent: {
		flexDirection: "row",
		alignItems: "center",
		marginTop: 40,
		marginLeft: 10,
	},
	stackNavegationIcon: {
		color: WHITE
	},
	stackNavegationText: {
		fontSize: 20,
		marginLeft: 10,
		color: WHITE
	},
})