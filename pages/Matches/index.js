import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  FlatList
} from 'react-native';

import api from '../../services/api';
import { userFriends } from '../../routes'
import { Storage, StorageKeys } from '../../services/storage';

import globalStyles from '../../assets/styles';
import styles from './style';
import CardItem from '../../components/CardItem';

const Matches = ( props ) => {
  const [matches, setMatches] = useState([{}]);
  
  useEffect(() => {
    const runner = async () => {
      const storedUser = await Storage.getData(StorageKeys.User.Info);
      const nextMatch = await api.get(userFriends, {
        headers: {
          id: storedUser._id
        }
      });
      setMatches(nextMatch.data.data);
    }

    runner();
  }, []);
  
  return (
    <ImageBackground
      source={require('../../assets/images/bg.png')}
      style={globalStyles.bg}
    >
      <View style={styles.containerMatches}>
        <ScrollView>
          <View style={globalStyles.top}>
            <Text style={globalStyles.title}>Amigos</Text>
          </View>

          <FlatList
            numColumns={2}
            data={matches}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item }) => (
              <TouchableOpacity
              onPress={ () => {
                props.navigation.navigate("MatcheProfile", { item }) 
              }}
              >
                <CardItem
                  image={item.image}
                  name={item.nome}
                  variant
                />
              </TouchableOpacity>
            )}
          />
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

export default Matches;
